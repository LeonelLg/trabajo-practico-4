import os
import time
os.system("cls")
def menu():
    print("''MENU''")
    print("1-Registrar Productos")
    print("2-Mostrar el Listado de Productos")
    print("3-Mostrar el Stock de Productos [desde,hasta]")
    print("4-Cargar Stock a los Productos menores que 'Y' Stock")
    print("5-Eliminar Todos los Productos con Stock Cero")
    print("6-Salir")
    x=int(input("Seleccione una opcion: "))
    while not(x>=1 and x<=6):
        print("''OPCION INCORRECTA''")
        x=int(input("Seleccione una opcion: "))
    os.system("cls")
    return x

def positivoStock():
    numero = int( input("Ingresar Stock: "))
    while numero <0:
        try:
            print("''ERROR'' el Stock debe ser positivo")
            numero = int(input("Ingresar Stock: "))
        except:
            pass
    return numero

def positivoPrecio():
    precio = float(input("Ingresar Precio: "))
    while precio<0:
        try:
            print("''ERROR'' el Precio debe ser positivo")
            precio = float(input("ingresar Precio: "))
        except:
            pass
    return precio

def cargarProducto():
    lista={}
    cont="s"
    while cont=="s" or cont=="S":
        codigo = int(input("Ingrese Codigo: "))
        if codigo not in lista:
            producto=input("Ingresar Producto: ")
            precio= positivoPrecio()
            stock= positivoStock()
            lista[codigo] = [producto,precio,stock]
        else:
            print("El Codigo Ingresado Existe")
            time.sleep(2)
        os.system("cls")
        cont = input("Desea Seguir Ingresando Productos s/n: ")
        os.system("cls")
    return lista

def mostrarDiccionario(lista):
    print("'Listado de Productos'")
    for clave, valor in lista.items():
        print("Clave: ",clave," Producto:",valor[0]," Precio: ",valor[1],"$  Stock: ",valor[2])
    input("Precione Cualquier Tecla Para Continuar")
    os.system("cls")

def desdeHasta(lista):
    desde=int(input("desde: "))
    hasata=int(input("hasta: "))
    os.system("cls")
    print("'Listado de Productos con Stock entre [",desde,",",hasata,"]'")
    for codigo, valor in lista.items():
            if (valor[2]>=desde and valor[2]<=hasata):
                print("codigo: ",codigo," producto: ", valor[0])
    input("Precione Cualquier Tecla Para Continuar")
    os.system("cls")

def agregarStock(lista):
    stockMenor=int(input("Ingresar el valor de 'y' Stock: "))
    for codigo, valor in lista.items():
            if (valor[2]<stockMenor):
                print("Clave: ",codigo," Producto:",valor[0]," Precio: ",valor[1],"$  Stock: ",valor[2])
                aumentarStock=int(input("¿Cuanto Agregar de Stock?: "))
                valor[2]=valor[2]+aumentarStock
                print("Se Aumento ",aumentarStock," de Stock a ",valor[0])
                input("Precione Cualquier Tecla Para Continuar")
                os.system("cls")

def buscarStockCero(lista):
    for codigo, valor in lista.items():
        if (valor[2]==0):
            print("Clave: ",codigo," Producto:",valor[0]," Precio: ",valor[1],"$  Stock: ",valor[2])
            return codigo
    return 0
            
def eliminarStock(lista):
    codigo=buscarStockCero(lista)
    while(codigo!=0):
        del lista[codigo]
        print("Eliminado")
        input("Precionar Enter para continuar")
        os.system("cls")
        codigo = buscarStockCero(lista)

#PRINCIPAL
continuar=0
cargar=0
while continuar!=6:
    opcion=menu()
    if (opcion==1):
        lista=cargarProducto()
        cargar=1
    elif(opcion==2 and cargar!=0):
        mostrarDiccionario(lista)
    elif(opcion==3 and cargar!=0):
        desdeHasta(lista)
    elif(opcion==4 and cargar!=0):
        agregarStock(lista)
    elif(opcion==5 and cargar!=0):
        eliminarStock(lista)
    elif(opcion==6):
        continuar=6
    else:
        print("Debe Cargar los Productos")